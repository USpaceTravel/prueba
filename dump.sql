--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-02-07 17:09:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3345 (class 1262 OID 16394)
-- Name: prueba; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE prueba WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Colombia.1252';


ALTER DATABASE prueba OWNER TO postgres;

\connect prueba

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3332 (class 0 OID 16395)
-- Dependencies: 209
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20220206201305', '2022-02-06 21:20:24', 40);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20220206224858', '2022-02-06 23:49:05', 30);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20220207140956', '2022-02-07 15:53:25', 29);
INSERT INTO public.doctrine_migration_versions (version, executed_at, execution_time) VALUES ('DoctrineMigrations\Version20220207173439', '2022-02-07 18:35:11', 252);


--
-- TOC entry 3338 (class 0 OID 16413)
-- Dependencies: 215
-- Data for Name: factura; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3334 (class 0 OID 16402)
-- Dependencies: 211
-- Data for Name: producto; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (1, 'papel', 'papel', '', 2500, 225, 'pp5520');
INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (2, 'Jugo hit', 'Jugo', NULL, 2000, 220, 'jhs454');
INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (3, 'Jugo hit', 'Jugo', 'aaaa', 2000, 220, 'jhs454');
INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (4, 'Jugo hit', 'Jugo', 'aaaa', 2000, 220, 'jhs454');
INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (6, 'Jugo valle', 'Jugo naranja', 'aaaa', 2000, 180, 'hgg');
INSERT INTO public.producto (id, nombre, descripcion, foto, precio, iva, sku) VALUES (7, 'Jugo valle', 'Jugo naranja', 'aaaa', 2000, 180, 'hgg');


--
-- TOC entry 3339 (class 0 OID 16420)
-- Dependencies: 216
-- Data for Name: venta; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 213
-- Name: factura_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.factura_id_seq', 1, false);


--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 210
-- Name: producto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.producto_id_seq', 7, true);


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 212
-- Name: producto_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.producto_id_seq1', 1, true);


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 214
-- Name: venta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.venta_id_seq', 1, false);


SET default_tablespace = '';

--
-- TOC entry 3182 (class 2606 OID 16400)
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- TOC entry 3186 (class 2606 OID 16419)
-- Name: factura factura_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.factura
    ADD CONSTRAINT factura_pkey PRIMARY KEY (id);


--
-- TOC entry 3184 (class 2606 OID 16409)
-- Name: producto producto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT producto_pkey PRIMARY KEY (id);


--
-- TOC entry 3190 (class 2606 OID 16424)
-- Name: venta venta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT venta_pkey PRIMARY KEY (id);


--
-- TOC entry 3187 (class 1259 OID 16425)
-- Name: idx_8fe7ee553de66465; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_8fe7ee553de66465 ON public.venta USING btree (idfactura_id);


--
-- TOC entry 3188 (class 1259 OID 16426)
-- Name: uniq_8fe7ee557645698e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_8fe7ee557645698e ON public.venta USING btree (producto_id);


--
-- TOC entry 3191 (class 2606 OID 16427)
-- Name: venta fk_8fe7ee553de66465; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT fk_8fe7ee553de66465 FOREIGN KEY (idfactura_id) REFERENCES public.factura(id);


--
-- TOC entry 3192 (class 2606 OID 16432)
-- Name: venta fk_8fe7ee557645698e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT fk_8fe7ee557645698e FOREIGN KEY (producto_id) REFERENCES public.producto(id);


-- Completed on 2022-02-07 17:09:36

--
-- PostgreSQL database dump complete
--

