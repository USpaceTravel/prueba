<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220207173439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE factura_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE venta_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE factura (id INT NOT NULL, cliente VARCHAR(255) NOT NULL, telefono BIGINT NOT NULL, email VARCHAR(255) NOT NULL, subtotal BIGINT NOT NULL, iva BIGINT NOT NULL, grantotal BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE venta (id INT NOT NULL, idfactura_id INT DEFAULT NULL, producto_id INT DEFAULT NULL, cantidad BIGINT NOT NULL, valoruni BIGINT NOT NULL, iva BIGINT NOT NULL, valortotal BIGINT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8FE7EE553DE66465 ON venta (idfactura_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8FE7EE557645698E ON venta (producto_id)');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT FK_8FE7EE553DE66465 FOREIGN KEY (idfactura_id) REFERENCES factura (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE venta ADD CONSTRAINT FK_8FE7EE557645698E FOREIGN KEY (producto_id) REFERENCES producto (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE venta DROP CONSTRAINT FK_8FE7EE553DE66465');
        $this->addSql('DROP SEQUENCE factura_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE venta_id_seq CASCADE');
        $this->addSql('DROP TABLE factura');
        $this->addSql('DROP TABLE venta');
    }
}
