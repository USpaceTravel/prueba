<?php

namespace App\Entity;

use App\Repository\FacturaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FacturaRepository::class)
 */
class Factura
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cliente;

    /**
     * @ORM\Column(type="bigint")
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="bigint")
     */
    private $subtotal;

    /**
     * @ORM\Column(type="bigint")
     */
    private $iva;

    /**
     * @ORM\Column(type="bigint")
     */
    private $grantotal;

    /**
     * @ORM\OneToMany(targetEntity=Venta::class, mappedBy="idfactura")
     */
    private $ventas;

    public function __construct()
    {
        $this->ventas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCliente(): ?string
    {
        return $this->cliente;
    }

    public function setCliente(string $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSubtotal(): ?string
    {
        return $this->subtotal;
    }

    public function setSubtotal(string $subtotal): self
    {
        $this->subtotal = $subtotal;

        return $this;
    }

    public function getIva(): ?string
    {
        return $this->iva;
    }

    public function setIva(string $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getGrantotal(): ?string
    {
        return $this->grantotal;
    }

    public function setGrantotal(string $grantotal): self
    {
        $this->grantotal = $grantotal;

        return $this;
    }

    /**
     * @return Collection|Venta[]
     */
    public function getIdproducto(): Collection
    {
        return $this->idproducto;
    }

    public function addIdproducto(Venta $idproducto): self
    {
        if (!$this->idproducto->contains($idproducto)) {
            $this->idproducto[] = $idproducto;
            $idproducto->setIdfactura($this);
        }

        return $this;
    }

    public function removeIdproducto(Venta $idproducto): self
    {
        if ($this->idproducto->removeElement($idproducto)) {
            // set the owning side to null (unless already changed)
            if ($idproducto->getIdfactura() === $this) {
                $idproducto->setIdfactura(null);
            }
        }

        return $this;
    }
}
