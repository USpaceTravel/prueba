<?php

namespace App\Entity;

use App\Repository\VentaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VentaRepository::class)
 */
class Venta
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=factura::class, inversedBy="idproducto")
     */
    private $idfactura;

    /**
     * @ORM\OneToOne(targetEntity=producto::class, cascade={"persist", "remove"})
     */
    private $producto;

    /**
     * @ORM\Column(type="bigint")
     */
    private $cantidad;

    /**
     * @ORM\Column(type="bigint")
     */
    private $valoruni;

    /**
     * @ORM\Column(type="bigint")
     */
    private $iva;

    /**
     * @ORM\Column(type="bigint")
     */
    private $valortotal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdfactura(): ?factura
    {
        return $this->idfactura;
    }

    public function setIdfactura(?factura $idfactura): self
    {
        $this->idfactura = $idfactura;

        return $this;
    }

    public function getProducto(): ?producto
    {
        return $this->producto;
    }

    public function setProducto(?producto $producto): self
    {
        $this->producto = $producto;

        return $this;
    }

    public function getCantidad(): ?string
    {
        return $this->cantidad;
    }

    public function setCantidad(string $cantidad): self
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getValoruni(): ?string
    {
        return $this->valoruni;
    }

    public function setValoruni(string $valoruni): self
    {
        $this->valoruni = $valoruni;

        return $this;
    }

    public function getIva(): ?string
    {
        return $this->iva;
    }

    public function setIva(string $iva): self
    {
        $this->iva = $iva;

        return $this;
    }

    public function getValortotal(): ?string
    {
        return $this->valortotal;
    }

    public function setValortotal(string $valortotal): self
    {
        $this->valortotal = $valortotal;

        return $this;
    }
}
