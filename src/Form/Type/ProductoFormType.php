<?php

namespace App\Form\Type;

use App\Entity\Producto;
use App\Form\Model\ProductoDTO;
use App\Form\Type\ProductoFormType as TypeProductoFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductoFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('sku', TextType::class)
            ->add('nombre', TextType::class)
            ->add('descripcion', TextType::class)
            ->add('foto', TextType::class)
            ->add('precio', NumberType::class)
            ->add('iva', NumberType::class)
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProductoDTO::class,
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string{

        return '';

    }

    public function getName(){

        return '';
    }
}