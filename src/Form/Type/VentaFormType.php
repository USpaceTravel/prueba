<?php

namespace App\Form\Type;

use App\Entity\Producto;
use App\Form\Model\VentaDTO;
use App\Form\Type\ProductoFormType as TypeProductoFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EnumType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VentaFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('idfactura', NumberType::class)
            ->add('producto', NumberType::class)
            ->add('cantidad', NumberType::class)
            ->add('valorunitario', NumberType::class)
            ->add('iva', NumberType::class)
            ->add('valortotal', NumberType::class)
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VentaDTO::class,
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix(): string{

        return '';

    }

    public function getName(){

        return '';
    }
}