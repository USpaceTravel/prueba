<?php

namespace App\Form\Model;

use App\Entity\Venta;

class VentaDTO{

    public $idfactura;
    public $producto;
    public $cantidad;
    public $valorunitario;
    public $iva;
    public $valortotal;

    public static function createFromVenta(Venta $venta):self {

        $dto = new self();
        $dto->idfactura = $venta->getidfactura();
        $dto->producto = $venta->getproducto();
        $dto->cantidad = $venta->getCantidad();
        $dto->valorunitario = $venta->getValoruni();
        $dto->iva = $venta->getIva();
        $dto->valortotal = $venta->getValortotal();
        return $dto;
    }
}