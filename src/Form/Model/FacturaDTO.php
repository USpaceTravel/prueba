<?php

namespace App\Form\Model;

use App\Entity\Factura;

class FacturaDTO{

    public $cliente;
    public $telefono;
    public $email;
    public $subtotal;
    public $iva;
    public $grantotal;
    public $ventas;

    public function __construct()
    {
        $this->ventas = [];
    }

    public static function createFromFactura(Factura $factura):self {

        $dto = new self();
        $dto->cliente = $factura->getCliente();
        $dto->telefono = $factura->getTelefono();
        $dto->email = $factura->getEmail();
        $dto->subtotal = $factura->getSubtotal();
        $dto->iva = $factura->getIva();
        $dto->grantotal = $factura->getGrantotal();
        return $dto;
    }
}