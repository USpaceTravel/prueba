<?php

namespace App\Form\Model;

use App\Entity\Producto;

class ProductoDTO{

    public $sku;
    public $nombre;
    public $descripcion;
    public $foto;
    public $precio;
    public $iva;

    public static function createFromProducto(Producto $producto):self {

        $dto = new self();
        $dto->sku = $producto->getSku();
        $dto->nombre = $producto->getNombre();
        $dto->descripcion = $producto->getDescripcion();
        $dto->foto = $producto->getFoto();
        $dto->precio = $producto->getPrecio();
        $dto->iva = $producto->getIva();
        return $dto;
    }
}

