<?php

namespace App\Controller\Api;

use App\Entity\Producto;
use App\Form\Model\ProductoDTO;
use App\Form\Type\ProductoFormType;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class ProductoController extends AbstractFOSRestController{

    /**
     * @Rest\Get(path="/productos") 
     * @Rest\View(serializerGroups={"producto"}, serializerEnableMaxDepthChecks=true)
     * */

    public function getAction(ProductoRepository $productoRepository){

        return $productoRepository->findAll();

    }

    /**
     * @Rest\Post(path="/productos") 
     * @Rest\View(serializerGroups={"producto"}, serializerEnableMaxDepthChecks=true)
     * */

    public function postAction(EntityManagerInterface $em, Request $request){

        $productoDto = new ProductoDTO();
        
        $form = $this->createForm(ProductoFormType::class, $productoDto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $producto = new Producto();
            $producto->setSku($productoDto->sku);
            $producto->setNombre($productoDto->nombre);
            $producto->setDescripcion($productoDto->descripcion);
            $producto->setFoto($productoDto->foto);
            $producto->setPrecio($productoDto->precio);  
            $producto->setIva($productoDto->precio*0.09); 
            
            $em->persist($producto);
            $em->flush();
            return $producto;   
        }
        return $form;

    }

    /**
     * @Rest\Post(path="/productos/edit/{id}", requirements={"id"="\d+"}) 
     * @Rest\View(serializerGroups={"producto"}, serializerEnableMaxDepthChecks=true)
     * */

    public function editAction(EntityManagerInterface $em, Request $request, int $id, ProductoRepository $productoRepository){

        $producto = $productoRepository->find($id);

        if (!$producto) {
            throw $this->createNotFoundException('Producto no encontrado');
        }

        $productoDto = ProductoDTO::createFromProducto($producto);

        $form = $this->createForm(ProductoFormType::class, $productoDto);   
        $form->handleRequest($request);
        if (!$form->isSubmitted()) {
            return new Response('ERROR');
        }

        if ($form->isValid()) {
            
            $producto->setSku($productoDto->sku);
            $producto->setNombre($productoDto->nombre);
            $producto->setDescripcion($productoDto->descripcion);
            $producto->setFoto($productoDto->foto);
            $producto->setPrecio($productoDto->precio);  
            $producto->setIva($productoDto->precio*0.09);  
            

            $em->persist($producto);
            $em->flush();
            return $producto;
        }
        return $form;

    }

    /**
     * @Rest\Post(path="/productos/delete/{id}", requirements={"id"="\d+"}) 
     * @Rest\View(serializerGroups={"producto"}, serializerEnableMaxDepthChecks=true)
     * */

    public function deleteAction(EntityManagerInterface $em, Request $request, int $id, ProductoRepository $productoRepository){

        $producto = $productoRepository->find($id);
        

        if (!$producto) {
            throw $this->createNotFoundException('Producto no encontrado');
        }

        $em->remove($producto);
        $em->flush();
        
        
        return $producto;

    }

}