<?php

namespace App\Controller\Api;

use App\Entity\Factura;
use App\Entity\Venta;
use App\Form\Model\FacturaDTO;
use App\Form\Model\VentaDTO;
use App\Form\Type\FacturaFormType;
use App\Form\Type\VentaFormType;
use App\Repository\FacturaRepository;
use App\Repository\ProductoRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;


class FacturaController extends AbstractFOSRestController{

    /**
     * @Rest\Get(path="/facturas") 
     * @Rest\View(serializerGroups={"factura"}, serializerEnableMaxDepthChecks=true)
     * */

    public function getAction(FacturaRepository $facturaRepository){

        return $facturaRepository->findAll();

    }

    /**
     * @Rest\Post(path="/facturas") 
     * @Rest\View(serializerGroups={"factura"}, serializerEnableMaxDepthChecks=true)
     * */

    public function postAction(EntityManagerInterface $em, Request $request, ProductoRepository $productoRepository, FacturaRepository $facturaRepository){

        $facturaDto = new FacturaDTO();
                
        $form = $this->createForm(FacturaFormType::class, $facturaDto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $factura = new Factura();
            $factura->setCliente($facturaDto->cliente);
            $factura->setTelefono($facturaDto->telefono);
            $factura->setemail($facturaDto->email);

                            
            $em->persist($factura);
            $em->flush();

            $valores = 0;

            foreach ($facturaDto->ventas as $ventas){

                $venta = new Venta();
                
                $venta->setIdfactura($factura->getId);
                $venta->setProducto($ventas->getProducto);
                
                $producto = $productoRepository->find($ventas->getProducto);
                if (!$producto) {
                    throw $this->createNotFoundException('Producto no encontrado');
                }
                
                $venta->setCantidad($ventas->getCantidad);
                $venta->setValoruni($producto->getPrecio);
                $venta->setValortotal($producto->getPrecio* $ventas->getCantidad);
                $venta->setIva($venta->getValortotal*0.09);

                $valores = $valores + $venta->getValortotal;
                $em->persist($venta);
                $em->flush();

            }
            $facturaUpdate = $facturaRepository->find($factura->getId);

            if (!$producto) {
                throw $this->createNotFoundException('factura no encontrado');
            }
   

            $facturaUpdate->setSubtotal($valores); 
            $facturaUpdate->setIva($valores*0.09); 
            $facturaUpdate->setGrantotal($valores*0.09+ $valores); 

            $em->persist($facturaUpdate);
            $em->flush();

            return $facturaUpdate;   
        }
        return $form;

    }

    

}